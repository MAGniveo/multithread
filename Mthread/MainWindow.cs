﻿using System;
using Gtk;
using Mono.Posix;
using Mono.Unix.Native;
using System.Diagnostics;
using System.ComponentModel;
using System.Text;
using Microsoft;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.IO;
using Mthread;

public partial class MainWindow: Gtk.Window
{
	Semaphore pool;
	class TCalcPi
	{
		public class myThread{
			Thread tr;
			public int kn;
		public	myThread(int m){
				tr=new Thread(SolveThread);
				tr.Start(m);

			}


			void SolveThread(object num){
				double x,y;
				Random rnd=new Random();
				for(int i=(int)num;i<=(int)num;i++){
					x = rnd.NextDouble(); // сгенерить случайные координаты точки
					y = rnd.NextDouble(); // 
					if (((x -  0.5) * (x -  0.5) + (y -  0.5) * (y -  0.5)) <  0.25)
					{
						this.kn++;
					}
				}
			}
		}

		int N;
		double aPi;
		public StringBuilder Msg; // диагностическое сообщение
		double x,y;
		int N_0=0;
		int k,k2=0;
		int itr;
		Random rnd;

		public void Init(int aN,int _itr)
		{
			N = aN; // сохранить общее количество вычислений
			aPi = -1; // флаг, что вычислений еще не было
			Msg = new StringBuilder("Вычислений не производилось");
			itr = _itr;
		}

		public void Run()
		{
			if (N < 1)
			{
				Msg = new StringBuilder("Некорректное значение N");
				return;
			}

			myThread thr=new myThread(0);
			Thread tr;
			var cancelToken = new CancellationTokenSource();
			//Task.Factory.StartNew(() => DoEthernalWork(), cancelToken.Token);

			List<Thread>trList=new List<Thread>();
			for(int i=0;i<itr;i++){
				tr = new Thread (SolveThread);
				trList.Add (tr);
			}

			for (int i = 0; i < itr; i++) {
				trList [i].Start(N/itr);
			}
			//cancelToken.Cancel (false);

			for (int i = 0; i < itr; i++) {
				trList [i].Join();
			}

			for(int i=0;i<N;i++){
				thr = new myThread (N);
			}

			N_0 +=thr.kn ;
			N_0+=k;

			aPi = 4.0 * ((double)N_0 / (double)N); // вычислить приближенное значение Pi
		}
		public double Done()
		{
			if (aPi >  0)
			{
				Msg = new StringBuilder("Вычисления успешно завершились");
				return aPi; // вернуть расчитанное значение
			}
			else
			{
				return  0; // нет результата
			}
		}
			
		public void SolveThread(object num){
		//	int kn = 0;
			StreamWriter sr=new StreamWriter(@"C:\Image\" + Thread.CurrentThread.ManagedThreadId + ".txt");
			double x, y;
			Random rand = new Random ();
			for(int i=1;i<=(int)num;i++){
				 x = rand.NextDouble(); // сгенерить случайные координаты точки
				 y = rand.NextDouble(); // 
				if (((x -  0.5) * (x -  0.5) + (y -  0.5) * (y -  0.5)) <  0.25)
				{
					k++;
				}

				Console.WriteLine (Thread.CurrentThread.ManagedThreadId+"::"+k);
			//if (File.Exists (@"C:\Image\" + Thread.CurrentThread.ManagedThreadId + ".txt")) {
				sr.WriteLine (Thread.CurrentThread.ManagedThreadId + "::" + k);
			}
			//sr.Close ();
		}
		public void SolveThread2(){

			StreamWriter sr=new StreamWriter(@"C:\Image\" + Thread.CurrentThread.ManagedThreadId + ".txt");
			for(int i=N/2;i<=N;i++){
				x = rnd.NextDouble(); // сгенерить случайные координаты точки
				y = rnd.NextDouble(); // 
				if (((x -  0.5) * (x -  0.5) + (y -  0.5) * (y -  0.5)) <  0.25)
				{
					k++;
				}

				Console.WriteLine (Thread.CurrentThread.ManagedThreadId+"::"+k);
				//if (File.Exists (@"C:\Image\" + Thread.CurrentThread.ManagedThreadId + ".txt")) {
				sr.WriteLine (Thread.CurrentThread.ManagedThreadId + "::" + k);				
			}
			sr.Close ();
		}

	}

	public MainWindow () : base (Gtk.WindowType.Toplevel)
	{
		Build ();
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}

	protected void ClickButtonFork (object sender, EventArgs e)
	{
		StringBuilder str=new StringBuilder();
		str.Append (@"C:\Image\HelloWorld\HelloWorld.exe");
		Process pr = new Process ();
		pr.StartInfo.UseShellExecute = true;
		pr.StartInfo.FileName = str.ToString();
		pr.StartInfo.CreateNoWindow = true;

		for(int i=0;i<5;i++){	
			pr.Start ();
			Console.WriteLine (pr.Id.ToString());
		}
	}

	protected void writeHelloWorld(object mt){
		Console.WriteLine ("Mutex start");

		Mutex mt1 = (Mutex) mt;
		mt1.WaitOne ();
		Console.WriteLine ("Hello World");
		mt1.ReleaseMutex();
	}

	void writeHelloWorld2(object br){
		Bariernew br1 = (Bariernew) br;
		Console.WriteLine ("Thread-- Wait All");
		br1.Wait ();
		Console.Write ("For Barier");
		pool.WaitOne ();
	}




	protected void ClickButtonMethodPi (object sender, EventArgs e)
	{

		int N;
		double Pi;

		if(!(Int32.TryParse(entry1.Text, out N)))
		{
			N =  0;
		}

		TCalcPi CalcPi = new TCalcPi();
		CalcPi.Init(N,Convert.ToInt32(entry2.Text));
		CalcPi.Run();
		Pi = CalcPi.Done();
		Console.WriteLine(" "+CalcPi.Msg.ToString());
		Console.WriteLine(" "+Pi);
		entry2.Text = Pi.ToString();


	}

	protected void ClickHelloWorld (object sender, EventArgs e)
	{
		Thread tr; 
		int k =Convert.ToInt32(entry1.Text);
		//ThreadPool.QueueUserWorkItem (new WaitCallback(writeHelloWorld));

		bool created;
		Mutex mt;
		Stack_stack<Thread> st = new Stack_stack<Thread>();
		mt = new Mutex (false , "Is run mutex " , out created );
		Bariernew _br = new Bariernew (k);
		pool = new Semaphore (0,k);

		for(int i=0;i<k;i++){

			//tr = new Thread (()=>_br.WriteNumber(i));
			tr = new Thread (writeHelloWorld2);
			st.Push (tr);
			st._array [i].Start (_br);

			/*tr = new Thread ();
			qt.Enqueue (tr);
			qt._array [i].Start ();*/

		}
		pool.Release (k);
		for (int i = 0; i < k; i++) {
			_br.Wait ();
			st._array [i].Join ();



		}

		st.mt.Close ();
		Console.WriteLine ("Godbye World");

	}

}
