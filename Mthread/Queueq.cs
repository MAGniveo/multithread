﻿using System;
using Mthread;

namespace Mthread
{
	public class Queueq<T>
	{
		public T[] _array;
		public int tail;
		public int head;
		public int count{ get; set ;}

		public Queueq ()
		{
			_array = new T[0];
		}
	
		public void Enqueue(T item)
		{
			if (count == _array.Length)
			{
				var capacity = (int) ((_array.Length*200L)/100L);
				if (capacity < (_array.Length + 4))
					capacity = _array.Length + 4;
				SetCapacity(capacity);
			}
			_array[tail] = item;
			tail = (tail + 1)%_array.Length;
			count++;
		}

		private void SetCapacity(int capacity)
		{
			var destinationArray = new T[capacity];
			if (count > 0)
			{
				if (head < tail)
					Array.Copy(_array, head, destinationArray, 0, count);
				else
				{
					Array.Copy(_array, head, destinationArray, 0, _array.Length - head);
					Array.Copy(_array, 0, destinationArray, _array.Length - head, tail);
				}
			}
			_array = destinationArray;
			head = 0;
			tail = (count == capacity) ? 0 : count;
		}

	}
}

