﻿using System;
using Mthread;
using System.Threading;

namespace Mthread
{
	public class Stack_stack<T>
	{
		public T[]_array;
		public int size{ get; set;}
		public Mutex mt;
		static bool created;
		public Bariernew br;
		public Stack_stack ()
		{
			this.size = 0;
			this._array = new T[10];
			mt = new Mutex (false , " Work " , out created );
		}

		public T Pop() 
		{
			if (this.size == 0)
			{ 
				throw new InvalidOperationException();
			}
			return this._array[--this.size];
		}

		public void Push(T newElement)
		{
			if (this.size == this._array.Length) 
			{ 
				T[] newArray = new T[2 * this._array.Length];
				Array.Copy(this._array, 0, newArray, 0, this.size);
				this._array = newArray; 
			} 
			this._array[this.size++] = newElement; 
		}

		public bool IsRun(){
			mt.ReleaseMutex ();
			return created;
		}

		public void Wait(){
			mt.WaitOne ();
		}

	}
}

