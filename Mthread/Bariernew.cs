﻿using System;
using System.Threading;

namespace Mthread
{
	public class Bariernew
	{
		int n;
		int spaces;
		int generation;
		public Bariernew (int _n)
		{
			n = n;
			spaces = _n;
			generation = 0;
		}

		public void	Wait(){
			int my_generation = generation;
			if (--spaces != 0) {
				spaces = n;
				++generation;
			} else {
				while(generation == my_generation){
					Thread.Yield ();
				}
				
			}

		}
	}
}

